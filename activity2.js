//ACTIVITY #2

db.new_users.insertMany([{"firstName":"Stephen", "lastName":"Hawking", "age":76, "email":"stephenhawking@mail.com", "department":"HR"},{"firstName":"Neil", "lastName":"Armstrong", "age":82, "email":"neilarmstrong@mail.com", "department":"HR"},{"firstName":"Bill", "lastName":"Gates", "age":65, "email":"billgates@mail.com", "department":"Operation"},{"firstName":"Jane", "lastName":"Doe", "age":21, "email":"janedoe@mail.com", "department":"HR"}])

db.new_users.find({$or:[{"firstName":{$regex:'s', $options:'$i'}},{"lastName":{$regex:'d', $options:'$i'}}]},{"_id":0, "firstName":1, "lastName":1});

db.new_users.find({$and:[{"department":"HR", "age":{$gte:70}}]});

db.new_users.find({$and:[{"firstName":{$regex:'e', $options:'$i'}},{"age":{$lte:30}}]})