db.inventory.insertMany([
	{
		"name": "Javascript for Beginners",
		"author": "James Doe",
		"price": 5000,
		"stocks": 50,
		"publisher": "JS Publishing House"
	},
	{
		"name": "HTML and CSS",
		"author": "John Thomas",
		"price": 2500,
		"stocks": 38,
		"publisher": "NY Publishers"
	},
	{
		"name": "Web Development Fundamentals",
		"author": "Noah Jimenez",
		"price": 3000,
		"stocks": 10,
		"publisher": "Big Idea Publishing House"
	},
	{
		"name": "Java Programming",
		"author": "David Michael",
		"price": 10000,
		"stocks": 100,
		"publisher": "JS Publishing House"
	}	
]);

//Comparison Query Operators
//$gt/$gte operator - greater than or greater than or equal
/*syntax: db.collectionName.find({ field:{$gt:value}})
			db.collectionName.find({ field:{$gt:value}})*/

db.inventory.find({"stocks":{$gt:50}});
db.inventory.find({"stocks":{$gte:50}});

//$lt or $lte operator
db.inventory.find({"stocks":{$lt:50}});
db.inventory.find({"stocks":{$lte:50}});

//$ne operator
db.inventory.find({"stocks":{$ne:50}})


//equal operator
db.inventory.find({"stocks": {$eq:50,}});
db.inventory.find({"stocks": 50});

//in operator
/*Syntax: db.collectionName.find({ field: { $in: [value1, value2 ]} })*/
db.inventory.find({"price":{$in:[10000, 5000]}})

//Logical Query Operator
//$and operator
/*db.collectionName.find({$and:[{fieldA:valueA},{fieldB:valueB}]})*/
db.inventory.find({$and:[{"stocks":{$ne:50}},{"price":{$ne:5000}}]})
db.inventory.find({"stocks": {$ne:50}, "price":{$ne:5000}})

//$or operator
db.inventory.find({$or:[{"name": "HTML and CSS"},{"publisher":"JS Publishing House"}]})
db.inventory.find({$or:[{"author":"James Doe"},{"price":{$lte:5000}}]})

//Field Projection-selecting specific field to project
//syntax: db.collectionName.find({criteria},{field:1}) -->1-inlcuded, 0-not included
db.inventory.find({"publisher":"JS Publishing House"},{"_id":0, "name":1, "author":1})
//_id - is the only field that can only included in a query in a 'exclusion operator' together with 'inclusion operator'

//Evaluation Query Operator
//$regex operator
/*syntax: db.collectionName.find({field:{$regex:'pattern', $options:'optionValue'}})*/
//Case Sensitive Query
db.inventory.find({"author":{$regex:'M'}})

//Case Insensitive Query
db.inventory.find({"author":{$regex:'M', $options:'$i'}})
//'$i'-->disregards the case of the letter